<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 5/24/2017
 * Time: 2:12 PM
 */

namespace App;


class Calculator
{
    protected $number1;
    protected $number2;
    protected $operation;

    public function setData($postArray){
        if(array_key_exists("Number1",$postArray)){
            $this->number1 = $postArray["Number1"];
        }
        if(array_key_exists("Number2",$postArray)){
            $this->number2 = $postArray["Number2"];
        }
        if(array_key_exists("Operation",$postArray)){
            $this->operation = $postArray["Operation"];
        }
    }
    public function calculate()
    {
        switch($this->operation)
        {
            case'Addition': return ($this->number1 + $this->number2);
            case'Subtraction': return ($this->number1 - $this->number2);
            case'Multiplication': return ($this->number1 * $this->number2);
            case'Division': return ($this->number1 / $this->number2);


        }
    }
}